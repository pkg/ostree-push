From: Andrej Shadura <andrew.shadura@collabora.co.uk>
Date: Mon, 18 Sep 2023 12:12:06 +0200
Subject: HACK: Only include references for commits before v2024

The ostree-push 0.x legacy protocol messages encoding uses a two bytes size
field in its header. The number of refs in the flatpak repository excess
this size and causes an overflow.

Work around the issue by not listing any ref including v2024 or newer,
since they use ostree-push 1.x.

Signed-off-by: Andrej Shadura <andrew.shadura@collabora.co.uk>
---
 scripts/ostree-receive-0 | 7 +++++++
 1 file changed, 7 insertions(+)

diff --git a/scripts/ostree-receive-0 b/scripts/ostree-receive-0
index 1100023..d8f4560 100755
--- a/scripts/ostree-receive-0
+++ b/scripts/ostree-receive-0
@@ -25,6 +25,7 @@ gi.require_version('OSTree', '1.0')
 from gi.repository import GLib, Gio, OSTree
 import logging
 import os
+import re
 import struct
 import subprocess
 import sys
@@ -119,6 +120,12 @@ class PushMessageWriter(object):
         cmdtype = PushCommandType.info
         mode = repo.get_mode()
         _, refs = repo.list_refs(None, None)
+        # HACK: Only include references for commits before v2024
+        refs = {
+            ref: commit for ref, commit in refs.items() if all(
+                year < 'v2024' for year in re.findall(r'v202[0-9]', ref)
+            )
+        }
         args = {
             'mode': GLib.Variant('i', mode),
             'refs': GLib.Variant('a{ss}', refs)
